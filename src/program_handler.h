#ifndef PROGRAM_HANDLER_H
#define PROGRAM_HANDLER_H


#include "thr_logic.h"
#include <memory>
#include <vector>

struct data
{
    //get from server
    double x;
    double y;
    double z;
    double current_time;

    //computed
    double vx;
    double vy;
    double vz;
    double abs;
    double abs_v;

};


class program_handler
{
public:
    program_handler();
    void print_data(struct data &d);
    void print_computed_data(struct data &d);
    std::shared_ptr<thr_logic>logic_main;
    std::shared_ptr<thr_logic>logic1;
    std::shared_ptr<thr_logic>logic2;
    std::vector<struct data> data;
};

#endif // PROGRAM_HANDLER_H
