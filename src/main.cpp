#include <stdio.h>
#include <curl/curl.h>
/* For older cURL versions you will also need
#include <curl/types.h>
#include <curl/easy.h>
*/
#include <string>
#include <iostream>

#include <random>
#include <fstream>
#include <limits>

#include <memory>


#include "curl_utils.h"
//our data and thread logic
#include "program_handler.h"

void downloadData(std::shared_ptr<program_handler> pr,std::shared_ptr<curl_utils> c,std::string myurl)
{
    while(true)
    {
        c->run(myurl,pr.get());

    }
    //pr->logic_main->run();
}

void processData(std::shared_ptr<program_handler> pr)
{
    static unsigned long counter=0;
    while(true)
    {

        pr->logic2->wait();

        std::cout<<"im in the printData="<<counter++<<std::endl;
        data d;
        if(!pr->data.empty() && pr->data.size()==2)
        {
            auto delta_time=pr->data[1].current_time-pr->data[0].current_time;

            d.x=pr->data[1].x;
            d.y=pr->data[1].y;
            d.z=pr->data[1].z;
            d.current_time=pr->data[1].current_time;


            d.vx=(pr->data[1].x-pr->data[0].x)/delta_time;
            d.vy=(pr->data[1].y-pr->data[0].y)/delta_time;
            d.vz=(pr->data[1].z-pr->data[0].z)/delta_time;

            d.abs=sqrt(d.x*d.x+d.y*d.y+d.z*d.z);
            d.abs_v=sqrt(d.vx*d.vx+d.vy*d.vy+d.vz*d.vz);




            std::cout<<"counter:"<<counter<<std::endl;
            pr->print_computed_data(d);
            counter++;
            pr->data.clear();
        }
        std::this_thread::sleep_for(std::chrono::seconds(1));


        pr->logic1->run();
        if(pr->logic_main->is_thread_ready==true)
        {
            break;
        }
    }


}


int main(int argc ,char **argv)
{
    auto c=std::make_shared<curl_utils>();

    std::string myurl="127.0.0.1:8000";
    if(argc<2)
    {
        auto f =std::make_shared<std::fstream>();
        std::cout << "Usage: python server.py" << std::endl;
        std::cout << "Usage: egor.exe <server>:<port>" << std::endl;
        std::cout << "by default: egor.exe 127.0.0.1:8000" << std::endl;

        return 0;
    }
    else
    {
        myurl=argv[1];
    }

    std::cout<<"starting 2 threads.. "<<std::endl;


    auto pr =std::make_shared<program_handler>();


    std::thread t1(downloadData,std::ref(pr),std::ref(c),std::ref(myurl));
    t1.detach();
    std::thread t2(processData,std::ref(pr));
    t2.detach();


    std::cout << "wait in main thread..." << std::endl;
    pr->logic_main->wait();

    std::cout << "program done computating" << std::endl;




    return 0;
}
