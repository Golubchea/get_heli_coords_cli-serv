#include "program_handler.h"

#include <iostream>

program_handler::program_handler()
{
    this->logic1=std::make_shared<thr_logic>();
    this->logic2=std::make_shared<thr_logic>();
    this->logic_main=std::make_shared<thr_logic>();
}

void program_handler::print_data(struct data &d)
{
    std::cout<<"data print"<<'\n';
    std::cout<<d.x<<'\n';
    std::cout<<d.y<<'\n';
    std::cout<<d.z<<'\n';
    std::cout<<d.current_time<<'\n';
}

void program_handler::print_computed_data(struct data &d)
{
    std::cout<<"--computed_data_print--"<<'\n';
    std::cout<<"velocity x   :"<<d.vx<<'\n';
    std::cout<<"velocity y   :"<<d.vy<<'\n';
    std::cout<<"velocity z   :"<<d.vz<<'\n';
    std::cout<<"current_time :"<<d.current_time<<'\n';
    std::cout<<"abs path     :"<<d.abs<<'\n';
    std::cout<<"abs_velocity :"<<d.abs_v<<'\n';
    std::cout<<"--computed_data_print-out--"<<'\n';
}
